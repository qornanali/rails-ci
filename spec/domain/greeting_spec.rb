require 'rails_helper'

describe Greeting do
  let(:hello){Greeting.new("upscale")}

  describe "#hai" do
    it 'should return hello' do
      expect(hello.hai).to eq("hello upscale")
    end

   it 'should executed correctly in my local env' do
      expect(hello.hai).to eq("hello upscale")
   end
  end

  describe "#hello" do
    it 'should return hai' do
      expect(hello.hello).to eq("hai upscale")
    end
  end
end
