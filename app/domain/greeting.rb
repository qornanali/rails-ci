class Greeting

  def initialize(name)
    @name = name
  end

  def hai
    "hello #{@name}"
  end

  def hello
    "hai #{@name}"
  end
end